data "aws_caller_identity" "current" {
}

locals {
  account_id          = data.aws_caller_identity.current.account_id
  issuer_host_path    = trim(var.issuer_url, "https://")
  provider_arn        = "arn:aws:iam::${local.account_id}:oidc-provider/${local.issuer_host_path}"
  service_account     = var.service_account
  service_account_arn = "system:serviceaccount:${var.kubernetes_namespace}:${local.service_account}"
}

data "aws_iam_policy_document" "oidc_assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [local.provider_arn]
    }

    condition {
      test     = "StringEquals"
      variable = "${local.issuer_host_path}:sub"
      values   = [local.service_account_arn]
    }
  }
}

data "aws_iam_policy_document" "route53_access" {
  statement {
    effect = "Allow"
    actions = [
      "route53:GetChange"
    ]
    resources = [
      "arn:aws:route53:::change/*"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets"
    ]
    resources = [
      "arn:aws:route53:::hostedzone/*"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "route53:ListHostedZonesByName"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "route53_access" {
  name        = "eks-${var.cluster_name}-cert-manager-route53-access"
  description = "EKS - Route53 access for cert-manager service ($var.cluster_name)"
  path        = "/"
  policy      = data.aws_iam_policy_document.route53_access.json
}

resource "aws_iam_role" "cert_manager" {
  name               = "eks-${var.cluster_name}-cert-manager"
  assume_role_policy = data.aws_iam_policy_document.oidc_assume.json
  path               = "/"
}

resource "aws_iam_role_policy_attachment" "route53_access" {
  role       = aws_iam_role.cert_manager.name
  policy_arn = aws_iam_policy.route53_access.arn
}

data "template_file" "cluster_issuer" {
  template = file("${path.module}/cluster-dns-issuer.tpl")

  vars = {
    region = var.aws_region
    zones  = join(",", var.allowed_zones_names)
  }
}

resource "local_file" "cluster_issuer" {
  count = (var.output_dir == "") ? 0 : 1

  filename = "${var.output_dir}/cluster-dns-issuer.yaml"
  content  = data.template_file.cluster_issuer.rendered
}
