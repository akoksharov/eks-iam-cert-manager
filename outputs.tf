output "iam_role_cert_manager_arn" {
  value = aws_iam_role.cert_manager.arn
}
