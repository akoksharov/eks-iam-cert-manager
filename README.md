# eks-iam-cert-manager

Configure IAM role and related policies to use EKS Service Accounts with the cert-manager service.

## Usage

```terraform
module "external_dns" {
  source               = "git::https://gitlab.com/akoksharov/eks-iam-cert-manager.git"
  cluster_name         = module.eks.cluster_name
  issuer_url           = module.eks.cluster_issuer_url
  kubernetes_namespace = "cert-manager"
  service_account      = "cert-manager"
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| allowed\_zones | List of zones this service account is permitted to update \(in ARN format\) | list(string) | `[ "arn:aws:route53:::hostedzone/*" ]` | no |
| allowed\_zones\_names | List of zones names this service account is permitted to update | list(string) | `[]` | no |
| cluster\_name |  | string | n/a | yes |
| issuer\_url | OIDC issuer URL \(include prefix\) | string | n/a | yes |
| kubernetes\_namespace | Namespace to operate in \(service accounts and pods must be in the same namespace\) | string | `"default"` | no |
| service\_account | Name of service account to create \(computed based on cluster name if not specified\) | string | `"default"` | no |
| aws_region | aws region | string | `"eu-west-1"` | no |
| output_dir | Directory where to save cluster-issuer manifest. If set to `""`, no file will be generated | string | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| iam\_role\_external\_dns\_arn |  |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
