##
# May be necessary to add the following argments to cert-manager
# --dns01-recursive-nameservers-only
# --dns01-recursive-nameservers="<coredns_cluster_ip>:53"

apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging-dns
spec:
  acme:
    email: info@lukapo.com
    privateKeySecretRef:
      name: letsencrypt-staging
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    solvers:
    - dns01:
        route53:
          region: ${region}
      selector:
        dnsZones: [${zones}]